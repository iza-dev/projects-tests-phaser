import { framesWalk, framesIdle, framesJump} from './frames/idle.js'

const config = {
    type: Phaser.AUTO,
    backgroundColor: '#d0f4f7',
    width: 800,
    height: 600,
    physics:{
        default: 'arcade'
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
}

/**
 * Players
 */
let girl 

let cursor

let boolIsMoving = false

const game = new Phaser.Game(config)

function preload ()
{
    for (let imgWalk = 1; imgWalk < 21; imgWalk++) {
        this.load.image(`girlWalk${imgWalk}`,`/assets/girl/Walk (${imgWalk}).png`)
    }
    for (let imgIdle = 1; imgIdle < 17; imgIdle++) {
        this.load.image(`girlIdle${imgIdle}`,`/assets/girl/Idle (${imgIdle}).png`)
    }
    for (let imgJump = 1; imgJump < 31; imgJump++) {
        this.load.image(`girlJump${imgJump}`,`/assets/girl/Jump (${imgJump}).png`)
    }

    // this.load.image(`bg`,`/assets/bg.png`)
}

function create ()
{
    // const bg = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'bg')
    // bg.setScale(4, 3) 

    this.anims.create({
        key: 'girlWalk',
        frames: framesWalk,
        repeat: -1,
        frameRate: 20
    })

    this.anims.create({
        key: 'girlIdle',
        frames: framesIdle,
        repeat: -1,
        frameRate: 20
    })

    this.anims.create({
        key: 'girlJump',
        frames: framesJump,
        repeat: -1,
        frameRate: 20
    })

    girl = this.physics.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'girl')
    girl.setScale(0.1) 

    girl.anims.play('girlIdle')

    //donne  des limites à la scene
    girl.body.collideWorldBounds = true

    cursor = this.input.keyboard.createCursorKeys()
}

function update (time, delta)
{
    girl.setVelocity(0)
    girl.anims.play('girlIdle', true)

    // if(cursor.up.isDown){
    //     boolIsMoving = true
    //     Move(0, -300, 'girlWalk')
    // }
    
    // if(cursor.down.isDown){
    //     boolIsMoving = true
    //     Move(0, 300, 'girlWalk')
    // }

    if(cursor.left.isDown){
        boolIsMoving = true
        Move(-300, 0, 'girlWalk', false, true)
    }

    if(cursor.right.isDown){
        boolIsMoving = true
        Move(300, 0, 'girlWalk')
    }
}

function  Move(velocityX, velocityY, anim, flipRight = false, flipLeft = false)
{
    girl.setVelocity(velocityX, velocityY)
    girl.anims.play(anim, false)
    girl.setFlip(flipLeft, flipRight)
}