const express = require('express')
require('dotenv').config()

const PORT = process.env.PORT || 8000

const app = express()

app
    .use(express.static(__dirname + '/public'))
    
    .get('/', (_, res)=>{
        res.sendFile(__dirname + '/index.html')
    })
    
    .listen(PORT, ()=>{
        console.log(`server ok on ${PORT}`)
    })